macro(grantlee_adjust_plugin_name pluginname)
    set_target_properties(${pluginname} PROPERTIES
        PREFIX ""
        LIBRARY_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/kf5/grantlee/${Grantlee5_VERSION_MAJOR}.${Grantlee5_VERSION_MINOR}"
        RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/kf5/grantlee/${Grantlee5_VERSION_MAJOR}.${Grantlee5_VERSION_MINOR}"
    )
endmacro()
