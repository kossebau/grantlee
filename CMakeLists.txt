cmake_minimum_required(VERSION 3.16)

set(KF_VERSION "5.87.0") # handled by release scripts
project(Grantlee5 VERSION ${KF_VERSION})
set(Grantlee5_MIN_PLUGIN_VERSION 0)
set(Grantlee5_MAJOR_MINOR_VERSION_STRING "${Grantlee5_VERSION_MAJOR}.${Grantlee5_VERSION_MINOR}" )

include(FeatureSummary)
find_package(ECM 5.86.0 CONFIG)
set_package_properties(ECM PROPERTIES
    TYPE REQUIRED
    DESCRIPTION "Extra CMake Modules."
    URL "https://commits.kde.org/extra-cmake-modules"
)

set(Grantlee_MODULE_DIR ${CMAKE_SOURCE_DIR}/cmake/modules)
set(CMAKE_MODULE_PATH ${ECM_MODULE_PATH} ${Grantlee_MODULE_DIR})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDEFrameworkCompilerSettings NO_POLICY_SCOPE)

include(ECMGenerateHeaders)
include(ECMAddQch)
include(ECMQtDeclareLoggingCategory)

include(CMakePackageConfigHelpers)
include(GenerateExportHeader)

option(BUILD_QCH "Build API documentation in QCH format (for e.g. Qt Assistant, Qt Creator & KDevelop)" OFF)
add_feature_info(QCH ${BUILD_QCH} "API documentation in QCH format (for e.g. Qt Assistant, Qt Creator & KDevelop)")

set(EXCLUDE_DEPRECATED_BEFORE_AND_AT 0 CACHE STRING "Control the range of deprecated API excluded from the build [default=0].")

option( BUILD_MAIN_PLUGINS "Build the Grantlee Templates plugins" TRUE )
option( BUILD_I18N_PLUGIN "Build the Grantlee Templates i18n plugin" TRUE )

set(REQUIRED_QT_VERSION 5.15.2)
find_package(Qt5Core ${REQUIRED_QT_VERSION} REQUIRED NO_MODULE)
find_package(Qt5Gui ${REQUIRED_QT_VERSION} REQUIRED NO_MODULE)
find_package(Qt5Qml ${REQUIRED_QT_VERSION} NO_MODULE)

set_package_properties(Qt5Qml PROPERTIES
    TYPE OPTIONAL
    PURPOSE "Build Javascript binding for Grantlee"
)
set_package_properties(Qt5Gui PROPERTIES
    TYPE OPTIONAL
    PURPOSE "Required to build TextDocument Grantlee library"
)

add_definitions(
    -DQT_DISABLE_DEPRECATED_BEFORE=0x050e00 # Not 050f00 because of QLinkedList
)

kde_enable_exceptions()

set(PLUGIN_INSTALL_DIR ${KDE_INSTALL_PLUGINDIR}/kf5/grantlee/${Grantlee5_MAJOR_MINOR_VERSION_STRING})

include("${Grantlee_MODULE_DIR}/KF5GrantleeMacros.cmake")

add_subdirectory(templates)

# forked by projects now, kjots & kpimtextedit
#add_subdirectory(textdocument)

feature_summary(WHAT ALL FATAL_ON_MISSING_REQUIRED_PACKAGES)
