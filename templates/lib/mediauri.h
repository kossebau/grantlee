/*
  Copyright 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRANTLEE_MEDIAURI_H
#define GRANTLEE_MEDIAURI_H

#include <QString>

namespace Grantlee
{

/// @headerfile mediauri.h grantlee/mediauri.h

/**
  @brief An URI for media.
*/
struct MediaUri
{
    /**
      The path of the media.
      Can be absolute or relative, needs to hold a trailing path separator if not empty.
    */
    QString path;

    /**
      The storage name of the media.
    */
    QString name;
};

}

Q_DECLARE_TYPEINFO(Grantlee::MediaUri, Q_MOVABLE_TYPE);

#endif
