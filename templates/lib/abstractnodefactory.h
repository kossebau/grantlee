/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_ABSTRACTNODEFACTORY_H
#define GRANTLEE_ABSTRACTNODEFACTORY_H

#include "grantlee_export.h"

#include <QObject>
#include <QStringList>

#include <memory>

namespace Grantlee
{
class Engine;
class FilterExpression;
class Parser;
class Node;

class AbstractNodeFactoryPrivate;

/// @headerfile abstractnodefactory.h grantlee/abstractnodefactory.h

/**
  @brief Base class for all NodeFactories

  This class can be used to make custom tags available to templates.
  The getNode method should be implemented to return a Node to be rendered.

  A node is represented in template markup as content surrounded by percent
  signed tokens.

  @code
    text content
    {% some_tag arg1 arg2 %}
      text content
    {% some_other_tag arg1 arg2 %}
      text content
    {% end_some_other_tag %}
    text content
  @endcode

  It is the responsibility of an **%AbstractNodeFactory** implementation to
  process the contents of a tag and return a Node implementation from its
  getNode method.

  The @ref getNode method would for example be called with the tagContent
  \"<tt>some_tag arg1 arg2</tt>\". That content could then be split up, the
  arguments processed and a Node created

  @code
    Node* SomeTagFactory::getNode(const QString &tagContent, Parser *p) {
      QStringList parts = smartSplit( tagContent );

      parts.removeFirst(); // Remove the "some_tag" part.

      FilterExpression arg1( parts.first(), p );
      FilterExpression arg2( parts.at( 1 ), p );

      return new SomeTagNode( arg1, arg2, p );
    }
  @endcode

  The @ref getNode implementation might also advance the parser. For example if
  we had a @gr_tag{times} tag which rendered content the amount of times it was
  given in its argument, it could be used like this:

  @code
    Some text content.
    {% times 5 %}
      the bit to be repeated
    {% end_times %}
    End text content
  @endcode

  The argument to @gr_tag{times} might not be a simple number, but could be a
  FilterExpression such as \"<tt>someobject.some_property|getDigit:1</tt>\".

  The implementation could look like

  @code
    Node* SomeOtherTagFactory::getNode(const QString &tagContent, Parser *p) {
      QStringList parts = smartSplit( tagContent );

      parts.removeFirst(); // Remove the "times" part.

      FilterExpression arg( parts.first(), p );

      auto node = new SomeTagNode( arg, p );
      auto childNodes = p->parse( node, "end_times" );
      node->setChildNodes( childNodes );
      p->removeNextToken();

      return node;
    }
  @endcode

  Note that it is necessary to invoke the parser to create the child nodes only
  after creating the Node to return. That node must be passed to the Parser to
  perform as the parent QObject to the child nodes.

  @see Parser::parse
*/
class GRANTLEE_EXPORT AbstractNodeFactory : public QObject
{
  Q_OBJECT
public:
  /**
    Constructor.

    @param parent The parent QObject
  */
  explicit AbstractNodeFactory(QObject *parent = {});

  /**
    Destructor.
  */
  ~AbstractNodeFactory() override;

  /**
    This method should be reimplemented to return a Node which can be
    rendered.

    The @p tagContent is the content of the tag including the tag name and
    arguments. For example, if the template content is @gr_tag{my_tag arg1
    arg2}, the tagContent will be &quot;my_tag arg1 arg2&quot;.

    The Parser @p p is available and can be advanced if appropriate. For
    example, if the tag has an end tag, the parser can be advanced to the end
    tag.

    @see tags
  */
  Q_REQUIRED_RESULT
  virtual Node *getNode(const QString &tagContent, Parser *p) const = 0;

#ifndef Q_QDOC
  /**
    @internal

    Sets the Engine which created this NodeFactory. Used by the
    ScriptableNodeFactory.
  */
  virtual void setEngine(Engine *) {}
#endif

protected:
  /**
    Splits @p str into a list, taking quote marks into account.

    This is typically used in the implementation of getNode with the
    tagContent.

    If @p str is 'one &quot;two three&quot; four 'five &quot; six' seven', the
    returned list will contain the following strings:

    - one
    - &quot;two three&quot;
    - four
    - five &quot; six
    - seven
  */
  Q_REQUIRED_RESULT
  Q_INVOKABLE QStringList smartSplit(const QString &str) const;

protected:
  /**
    Returns a list of FilterExpression objects created with Parser @p p as
    described by the content of @p list.

    This is used for convenience when handling the arguments to a tag.
  */
  Q_REQUIRED_RESULT
  QList<FilterExpression> getFilterExpressionList(const QStringList &list,
                                                  Parser *p) const;

private:
  Q_DECLARE_PRIVATE(AbstractNodeFactory)
  const std::unique_ptr<AbstractNodeFactoryPrivate> d_ptr;
};

}

#endif
