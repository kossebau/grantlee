/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nodelist.h"

#include "nodebuiltins_p.h"

using namespace Grantlee;

NodeList::NodeList() : QList<Grantlee::Node *>(), m_containsNonText(false) {}

NodeList::NodeList(const NodeList &list) : QList<Grantlee::Node *>(list)
{
  m_containsNonText = list.m_containsNonText;
}

NodeList &NodeList::operator=(const NodeList &list)
{
  static_cast<QList<Grantlee::Node *> &>(*this)
      = static_cast<QList<Grantlee::Node *>>(list);
  m_containsNonText = list.m_containsNonText;
  return *this;
}

NodeList::NodeList(const QList<Grantlee::Node *> &list)
    : QList<Grantlee::Node *>(list)
{
  for (Grantlee::Node *node : list) {
    auto textNode = qobject_cast<TextNode *>(node);
    if (!textNode) {
      m_containsNonText = true;
      return;
    }
  }
  m_containsNonText = false;
}

NodeList::~NodeList() {}

void NodeList::append(Grantlee::Node *node)
{
  if (!m_containsNonText) {
    auto textNode = qobject_cast<TextNode *>(node);
    if (!textNode)
      m_containsNonText = true;
  }

  QList<Grantlee::Node *>::append(node);
}

void NodeList::append(const QList<Grantlee::Node *> &nodeList)
{
  if (!m_containsNonText) {
    for (Grantlee::Node *node : nodeList) {
      auto textNode = qobject_cast<TextNode *>(node);
      if (!textNode) {
        m_containsNonText = true;
        break;
      }
    }
  }

  QList<Grantlee::Node *>::append(nodeList);
}

bool NodeList::containsNonText() const { return m_containsNonText; }

void NodeList::render(OutputStream *stream, Context *c) const
{
  for (auto i = 0; i < this->size(); ++i) {
    this->at(i)->render(stream, c);
  }
}
