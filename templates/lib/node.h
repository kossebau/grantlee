/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_NODE_H
#define GRANTLEE_NODE_H

#include "context.h"
#include "grantlee_export.h"
#include "outputstream.h"
#include "safestring.h"

#include <memory>

namespace Grantlee
{

class TemplateImpl;

class NodePrivate;

/// @headerfile node.h grantlee/node.h

/**
  @brief Base class for all nodes.

  The **%Node** class can be implemented to make additional functionality
  available to Templates.

  A node is represented in template markup as content surrounded by percent
  signed tokens.

  @code
    text content
    {% some_tag arg1 arg2 %}
      text content
    {% some_other_tag arg1 arg2 %}
      text content
    {% end_some_other_tag %}
    text content
  @endcode

  This is parsed into a tree of **%Node** objects by an implementation of
  AbstractNodeFactory. The **%Node** objects can then later be rendered by their
  @ref render method.

  Rendering a **%Node** will usually mean writing some output to the stream. The
  content written to the stream could be determined by the arguments to the tag,
  or by the content of child nodes between a start and end tag, or both.

  @see FilterExpression
  @see @ref tags

  @author Stephen Kelly <steveire@gmail.com>
*/
class GRANTLEE_EXPORT Node : public QObject
{
  Q_OBJECT
public:
  /**
    Constructor.

    @param parent The parent QObject
  */
  explicit Node(QObject *parent = {});

  /**
    Destructor.
  */
  ~Node() override;

  /**
    Reimplement this to render the template in the Context @p c.

    This will also involve calling render on and child nodes.
  */
  virtual void render(OutputStream *stream, Context *c) const = 0;

#ifndef Q_QDOC
  /**
    @internal
  */
  Q_REQUIRED_RESULT
  virtual bool mustBeFirst()
  { // krazy:exclude:inline
    return false;
  }
#endif

protected:
  /**
    Renders the value @p input in the Context @p c. This will involve escaping
    @p input if necessary.

    This is only relevant to developing template tags.
  */
  void streamValueInContext(OutputStream *stream, const QVariant &input,
                            Grantlee::Context *c) const;

  /**
    Returns a raw pointer to the Template this **%Node** is in.
  */
  Q_REQUIRED_RESULT
  TemplateImpl *containerTemplate() const;

private:
  Q_DECLARE_PRIVATE(Node)
  const std::unique_ptr<NodePrivate> d_ptr;
};

}

#endif
