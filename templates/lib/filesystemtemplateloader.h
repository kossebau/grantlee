/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_FILESYSTEMTEMPLATELOADER_H
#define GRANTLEE_FILESYSTEMTEMPLATELOADER_H

#include "abstracttemplateloader.h"

#include <memory>

namespace Grantlee
{

/// @headerfile filsystemtemplateloader.h grantlee/filesystemtemplateloader.h

class AbstractLocalizer;

class FileSystemTemplateLoaderPrivate;

/**
  @brief The **%FileSystemTemplateLoader** loads Templates from the file system.

  This template loader works by traversing a list of directories to find
  templates. Directories are checked in order, and the first match hit is parsed
  and returned.

  @code
    loader->setTemplateDirs({
        "/home/user/app/templates",
        "/usr/local/share/app/templates"
    });
    engine->setTemplateLoader( loader );

    // This will try /home/user/app/templates/mytemplate.html
    // followed by /usr/local/share/app/templates/mytemplate.html
    engine->loadByName( "mytemplate.html" );
  @endcode

  Additionally, a themeName may be set on the template loader, which will be
  appended to search paths before the template name.

  @code
    loader->setTemplateDirs({
      "/home/user/app/templates" <<
      "/usr/local/share/app/templates"
    });
    loader->setTheme( "simple_theme" );
    engine->setTemplateLoader( loader );

    // This will try /home/user/app/templates/simple_theme/mytemplate.html
    // followed by /usr/local/share/app/templates/simple_theme/mytemplate.html
    engine->loadByName( "mytemplate.html" );
  @endcode

  Media URIs may be retrieved for media relative to the directories searched
  queried for templates.

  @code
    loader->setTemplateDirs({
      "/home/user/app/templates",
      "/usr/local/share/app/templates"
    });
    loader->setTheme( "simple_theme" );
    engine->setTemplateLoader( loader );

    // This will try /home/user/app/templates/simple_theme/logo.png
    // followed by /usr/local/share/app/templates/simple_theme/logo.png
    // and return the first one that exists.
    engine->mediaUri( "logo.png" );
  @endcode

  The template files loaded by a %**FileSystemTemplateLoader** must be UTF-8
  encoded.

  @see @ref deploying_templates

*/
class GRANTLEE_EXPORT FileSystemTemplateLoader
    : public AbstractTemplateLoader
{
public:
  /**
    Constructor
  */
  FileSystemTemplateLoader(const std::shared_ptr<AbstractLocalizer> &localizer
                           = {});

  /**
    Destructor
  */
  ~FileSystemTemplateLoader() override;

  Q_REQUIRED_RESULT
  Template loadByName(const QString &name, Engine const *engine) const override;

  Q_REQUIRED_RESULT
  bool canLoadTemplate(const QString &name) const override;

  Q_REQUIRED_RESULT
  MediaUri getMediaUri(const QString &fileName) const override;

  /**
    Sets the theme of this loader to @p themeName
  */
  void setTheme(const QString &themeName);

  /**
    The themeName of this TemplateLoader
  */
  Q_REQUIRED_RESULT
  QString themeName() const;

  /**
    Sets the directories to look for template files to @p dirs.
  */
  void setTemplateDirs(const QStringList &dirs);

  /**
    The directories this TemplateLoader looks in for template files.
   */
  Q_REQUIRED_RESULT
  QStringList templateDirs() const;

private:
  Q_DECLARE_PRIVATE(FileSystemTemplateLoader)
  const std::unique_ptr<FileSystemTemplateLoaderPrivate> d_ptr;
};

}

#endif
