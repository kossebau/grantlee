/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_TEMPLATELOADER_H
#define GRANTLEE_TEMPLATELOADER_H

#include "grantlee_export.h"
#include "template.h"
#include "mediauri.h"

namespace Grantlee
{

/// @headerfile abstracttemplateloader.h grantlee/abstracttemplateloader.h

/**
  @brief An retrieval interface to a storage location for Template objects.

  This interface can be implemented to define new ways of retrieving the content
  of Templates.

  The interface of this class should not be called directly from applications.
  TemplateLoaders will typically be created, configured and added to the
  Grantlee::Engine which will call the appropriate API.

  @author Stephen Kelly <steveire@gmail.com>
*/
class GRANTLEE_EXPORT AbstractTemplateLoader
{
public:
  /**
    Destructor
  */
  virtual ~AbstractTemplateLoader();

  /**
    Load a Template called @p name. Return an invalid Template if no content
    by that name exists.
  */
  Q_REQUIRED_RESULT
  virtual Template loadByName(const QString &name,
                              Engine const *engine) const = 0;

  /**
    Return a complete URI for media identified by fileName.
  */
  Q_REQUIRED_RESULT
  virtual MediaUri getMediaUri(const QString &fileName) const = 0;

  /**
    Return true if a Template identified by @p name exists and can be loaded.
  */
  Q_REQUIRED_RESULT
  virtual bool canLoadTemplate(const QString &name) const = 0;
};

}

#endif
