/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_INMEMORYTEMPLATELOADER_H
#define GRANTLEE_INMEMORYTEMPLATELOADER_H

#include "abstracttemplateloader.h"

#include <memory>

namespace Grantlee
{

class InMemoryTemplateLoaderPrivate;

/// @headerfile inmemorytemplateloader.h grantlee/inmemorytemplateloader.h

/**
  @brief The **%InMemoryTemplateLoader** loads Templates set dynamically in
  memory

  This class is mostly used for testing purposes, but can also be used for
  simple uses of %Grantlee.

  Templates can be made available using the @ref setTemplate method, and will
  then be retrieved by the Grantlee::Engine as appropriate.
*/
class GRANTLEE_EXPORT InMemoryTemplateLoader
    : public AbstractTemplateLoader
{
public:
  InMemoryTemplateLoader();
  ~InMemoryTemplateLoader() override;

  Q_REQUIRED_RESULT
  Template loadByName(const QString &name, Engine const *engine) const override;

  Q_REQUIRED_RESULT
  bool canLoadTemplate(const QString &name) const override;

  Q_REQUIRED_RESULT
  MediaUri getMediaUri(const QString &fileName) const override;

  /**
    Add a template content to this Loader.

    Example:

    @code
      auto loader = std::shared_ptr<InMemoryTemplateLoader::create();
      loader->setTemplate( "name_template", "My name is {{ name }}" );
      loader->setTemplate( "age_template", "My age is {{ age }}" );
      engine->addTemplateLoader( loader );

      // Both templates may now be retrieved by calling Engine::loadByName.
    @endcode
  */
  void setTemplate(const QString &name, const QString &content);

private:
  Q_DECLARE_PRIVATE(InMemoryTemplateLoader)
  const std::unique_ptr<InMemoryTemplateLoaderPrivate> d_ptr;
};

}

#endif
