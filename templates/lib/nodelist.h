/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GRANTLEE_NODELIST_H
#define GRANTLEE_NODELIST_H

// krazy:excludeall=dpointer

#include "node.h"

namespace Grantlee
{

/// @headerfile nodelist.h grantlee/nodelist.h

/**
  @brief A list of Nodes with some convenience API for rendering them.

  Typically, tags which have an end tag will create and later render a list of
  child nodes.

  This class contains API such as @ref append and @ref render to make creating
  such list easily.

  The @ref findChildren method behaves similarly to the QObject::findChildren
  method, returning a list of nodes of a particular type from the Node objects
  contained in the list (and their children).

  @see @ref tags_with_end_tags
*/
class GRANTLEE_EXPORT NodeList : public QList<Grantlee::Node *>
{
public:
  /**
    Creates an empty **%NodeList**.
  */
  NodeList();

  /**
    Copy constructor.
  */
  NodeList(const NodeList &list);

  NodeList &operator=(const NodeList &list);

  /**
    Convenience constructor
  */
  /* implicit */ NodeList(const QList<Grantlee::Node *> &list);

  /**
    Destructor.
  */
  ~NodeList();

  /**
    Appends @p node to the end of this **%NodeList**.
  */
  void append(Grantlee::Node *node);

  /**
    Appends @p nodeList to the end of this **%NodeList**.
  */
  void append(const QList<Grantlee::Node *> &nodeList);

  /**
    Returns true if this **%NodeList** contains non-text nodes.
  */
  Q_REQUIRED_RESULT
  bool containsNonText() const;

  /**
    A recursive listing of nodes in this tree of type @p T.
  */
  template <typename T>
  Q_REQUIRED_RESULT
  QList<T> findChildren()
  {
    QList<T> children;
    QList<Grantlee::Node *>::const_iterator it;
    const QList<Grantlee::Node *>::const_iterator first = constBegin();
    const QList<Grantlee::Node *>::const_iterator last = constEnd();
    for (it = first; it != last; ++it) {
      T object = qobject_cast<T>(*it);
      if (object) {
        children << object;
      }
      children << (*it)->findChildren<T>();
    }
    return children;
  }

  /**
    Renders the list of Nodes in the Context @p c.
  */
  void render(OutputStream *stream, Context *c) const;

private:
  bool m_containsNonText;
};

}

#endif
