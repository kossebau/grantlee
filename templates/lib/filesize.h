/*
  Copyright 2021 Friedrich W. H. Kossebau <kossebau@kde.org>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GRANTLEE_FILESIZE_H
#define GRANTLEE_FILESIZE_H

#include <QString>

namespace Grantlee
{

/// @headerfile filesize.h grantlee/filesize.h

/**
  @brief The size of a file.
*/
struct FileSize
{
    /**
      The value.
    */
    qreal value;

    /**
      The unit as string.
    */
    QString unit;
};

}

Q_DECLARE_TYPEINFO(Grantlee::FileSize, Q_MOVABLE_TYPE);

#endif
