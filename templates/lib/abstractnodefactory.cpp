/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "abstractnodefactory.h"

#include "filterexpression.h"

#include <QRegularExpressionMatchIterator>

using namespace Grantlee;

namespace Grantlee
{
class AbstractNodeFactoryPrivate
{
  AbstractNodeFactoryPrivate(AbstractNodeFactory *factory) : q_ptr(factory)
  {
#if defined(Q_CC_MSVC)
// MSVC doesn't like static string concatenations like L"foo" "bar", as
// results from QStringLiteral, so use QLatin1String here instead.
#define STRING_LITERAL QLatin1String
#else
#define STRING_LITERAL QStringLiteral
#endif
    smartSplitRe = QRegularExpression(
        STRING_LITERAL("("                  // match
                       "(?:[^\\s\\\'\\\"]*" // things that are not whitespace or
                                            // escaped quote chars
                       "(?:"                // followed by
                       "(?:\""              // Either a quote starting with "
                       "(?:[^\"\\\\]|\\\\.)*\"" // followed by anything that is
                                                // not the end of the quote
                       "|\'"                    // Or a quote starting with '
                       "(?:[^\'\\\\]|\\\\.)*\'" // followed by anything that is
                                                // not the end of the quote
                       ")"                      // (End either)
                       "[^\\s\'\"]*" // To the start of the next such fragment
                       ")+"          // Perform multiple matches of the above.
                       ")"           // End of quoted string handling.
                       "|\\S+"       // Apart from quoted strings, match
                                     // non-whitespace fragments also
                       ")"           // End match
                       ));

#undef STRING_LITERAL
  }

  Q_DECLARE_PUBLIC(AbstractNodeFactory)
  AbstractNodeFactory *const q_ptr;

public:
  QRegularExpression smartSplitRe;
};

}


AbstractNodeFactory::AbstractNodeFactory(QObject *parent)
    : QObject(parent), d_ptr(new AbstractNodeFactoryPrivate(this))
{
}

AbstractNodeFactory::~AbstractNodeFactory() = default;

QList<FilterExpression>
AbstractNodeFactory::getFilterExpressionList(const QStringList &list,
                                             Parser *p) const
{
  QList<FilterExpression> fes;
  for (auto &varString : list) {
    fes << FilterExpression(varString, p);
  }
  return fes;
}

QStringList AbstractNodeFactory::smartSplit(const QString &str) const
{
  Q_D(const AbstractNodeFactory);
  QStringList l;

  auto i = d->smartSplitRe.globalMatch(str);
  while (i.hasNext()) {
    auto match = i.next();
    l.append(match.captured());
  }

  return l;
}
