/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "inmemorytemplateloader.h"

#include "engine.h"

using namespace Grantlee;

namespace Grantlee
{
class InMemoryTemplateLoaderPrivate
{
public:
    QHash<QString, QString> m_namedTemplates;
};
}

InMemoryTemplateLoader::InMemoryTemplateLoader()
    : AbstractTemplateLoader()
    , d_ptr(new InMemoryTemplateLoaderPrivate())
{
}

InMemoryTemplateLoader::~InMemoryTemplateLoader() = default;

void InMemoryTemplateLoader::setTemplate(const QString &name,
                                         const QString &content)
{
    Q_D(InMemoryTemplateLoader);

    d->m_namedTemplates.insert(name, content);
}

bool InMemoryTemplateLoader::canLoadTemplate(const QString &name) const
{
    Q_D(const InMemoryTemplateLoader);

    return d->m_namedTemplates.contains(name);
}

Template InMemoryTemplateLoader::loadByName(const QString &name,
                                            Engine const *engine) const
{
    Q_D(const InMemoryTemplateLoader);

    const auto it = d->m_namedTemplates.constFind(name);
    if (it != d->m_namedTemplates.constEnd()) {
        return engine->newTemplate(it.value(), name);
    }
    throw Grantlee::Exception(
                TagSyntaxError,
                QStringLiteral("Couldn't load template %1. Template does not exist.")
                .arg(name));
}

MediaUri InMemoryTemplateLoader::getMediaUri(const QString &fileName) const
{
  Q_UNUSED(fileName)
  // This loader doesn't make any media available yet.
  return {};
}
