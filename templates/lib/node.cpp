/*
  This file is part of the Grantlee template system.

  Copyright (c) 2009,2010 Stephen Kelly <steveire@gmail.com>

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either version
  2.1 of the Licence, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "node.h"

#include "metaenumvariable_p.h"
#include "template.h"
#include "util.h"

using namespace Grantlee;

namespace Grantlee
{

class NodePrivate
{
  NodePrivate(Node *node) : q_ptr(node) {}
  Q_DECLARE_PUBLIC(Node)
  Node *const q_ptr;
};

}

Node::Node(QObject *parent) : QObject(parent), d_ptr(new NodePrivate(this)) {}

Node::~Node() = default;

void Node::streamValueInContext(OutputStream *stream, const QVariant &input,
                                Context *c) const
{
  Grantlee::SafeString inputString;
  if (input.userType() == qMetaTypeId<QVariantList>()) {
    inputString = toString(input.value<QVariantList>());
  } else if (input.userType() == qMetaTypeId<MetaEnumVariable>()) {
    const auto mev = input.value<MetaEnumVariable>();
    if (mev.value >= 0)
      (*stream) << QString::number(mev.value);
  } else {
    inputString = getSafeString(input);
  }
  if (c->autoEscape() && !inputString.isSafe())
    inputString.setNeedsEscape(true);

  (*stream) << inputString;
}

TemplateImpl *Node::containerTemplate() const
{
  auto _parent = parent();
  auto ti = qobject_cast<TemplateImpl *>(_parent);
  while (_parent && !ti) {
    _parent = _parent->parent();
    ti = qobject_cast<TemplateImpl *>(_parent);
  }
  Q_ASSERT(ti);
  return ti;
}
